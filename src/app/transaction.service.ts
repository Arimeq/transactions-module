import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { mockedData } from '../mock/transactions.json';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  data: any[];

  constructor() {
    this.data = mockedData.data;
  }

  getTransactionsList() {
    return of.apply(this, this.data);
  }

  saveTransaction(data: { amount: number, merchant: string }) {
    const transferCodes = ["#c12020", "#e25a2c", "#fbbb1b"];
    const transaction = {
      categoryCode: transferCodes[Math.floor(Math.random() * transferCodes.length)],
      dates: {
        valueDate: Date.now()
      },
      transaction: {
        amountCurrency: {
          amount: data.amount,
          currencyCode: "EUR"
        },
        type: "Online Transfer",
        creditDebitIndicator: "DBIT"
      },
      merchant: {
        name: data.merchant,
        accountNumber: ""
      }
    };
    this.data.unshift(transaction);
  }
}
