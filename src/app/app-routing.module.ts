import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { TransactionPreviewComponent } from './transaction-preview/transaction-preview.component';

const routes: Routes = [
  { path: 'main', component: MainComponent },
  { path: 'preview/:merchant/:amount', component: TransactionPreviewComponent },
  { path: '', redirectTo: 'main', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
