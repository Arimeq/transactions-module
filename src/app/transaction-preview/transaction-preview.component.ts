import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { TransactionService } from '../transaction.service';

@Component({
  selector: 'app-transaction-preview',
  templateUrl: './transaction-preview.component.html',
  styleUrls: ['./transaction-preview.component.css']
})
export class TransactionPreviewComponent implements OnInit {
  merchant: string | null = '';
  amount: string | null = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private transactionService: TransactionService
  ) {}

  ngOnInit() {
    this.merchant = this.route.snapshot.paramMap.get('merchant');
    this.amount = this.route.snapshot.paramMap.get('amount');
  }

  save() {
    this.transactionService.saveTransaction({
      amount: Number(this.amount),
      merchant: String(this.merchant)
    });
    this.router.navigate(['/main']);
  }

  cancel() {
    this.router.navigate(['/main']);
  }
}
