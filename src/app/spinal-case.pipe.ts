import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'spinalCase'
})
export class SpinalCasePipe implements PipeTransform {

  transform(value: String, ...args: unknown[]): String {
    return value.toLocaleLowerCase().replace(/\s/g, '-');
  }

}
