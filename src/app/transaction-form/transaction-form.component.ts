import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransactionService } from '../transaction.service';

@Component({
  selector: 'app-transaction-form',
  templateUrl: './transaction-form.component.html',
  styleUrls: ['./transaction-form.component.css']
})
export class TransactionFormComponent implements OnInit {
  merchant$ = '';
  amount$ = '';

  constructor(
    private router: Router,
    private transactionService: TransactionService
  ) { }

  ngOnInit(): void {
  }

  onSubmit(event: Event) {
    event.preventDefault();
    this.router.navigate(['/preview', this.merchant$, this.amount$]);
  }

  canSubmit(): boolean {
    return !(this.merchant$ && this.amount$);
  }

}
