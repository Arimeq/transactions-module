import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { TransactionService } from '../transaction.service';

@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.css']
})
export class TransactionsListComponent implements OnInit {
  transactions: any[] = [];
  dir: string = 'asc';
  sortedBy: string = '';
  filterStr: string = '';
  dateFormat: string = 'MMM d';
  datePipe: DatePipe = new DatePipe('en-US');

  constructor(private transactionService: TransactionService) {}

  ngOnInit(): void {
    this.transactionService
      .getTransactionsList()
      .subscribe(next => this.transactions.push(next));
  }

  private comparestrings(a: string, b: string): number {
    if (a.toLocaleLowerCase() < b.toLocaleLowerCase()) {
      return -1;
    }
    if (b.toLocaleLowerCase() < a.toLocaleLowerCase()) {
      return 1;
    }
    return 0;
  }

  buttonClassList(name: string) {
    if (name === this.sortedBy) {
      return `recent-transactions__sort-button recent-transactions__sort-button--${this.dir}`;
    }
    return `recent-transactions__sort-button`;
  }

  setFilter(event: any): void {
    this.filterStr = event.target.value;
  }

  clearFilter() {
    this.filterStr = '';
  }

  filtered(): any[] {
    return this.transactions.filter(t => {
      const { dates, transaction, merchant } = t;
      const date = this.datePipe.transform(dates.valueDate, this.dateFormat);
      const { amountCurrency, type } = transaction;
      const { amount } = amountCurrency;
      const { name } = merchant;
      const searchStr = `${date?.toLocaleLowerCase()}${type.toLocaleLowerCase()}${amount}${name.toLocaleLowerCase()}`;
      return searchStr.indexOf(this.filterStr.toLocaleLowerCase()) > -1;
    });
  }

  sortBy(param: string): void {
    if (this.sortedBy === param) {
      this.flipDir();
    }
    this.sortedBy = param;
    this.transactions.sort((a: any, b: any): number => {
      switch (param) {
        case 'date':
          const datea = new Date(a.dates.valueDate);
          const dateb = new Date(b.dates.valueDate);
          if (this.dir === 'asc') {
            return datea.getTime() - dateb.getTime();
          }
          return dateb.getTime() - datea.getTime();
        case 'amount':
          const amounta = a.transaction.amountCurrency.amount;
          const amountb = b.transaction.amountCurrency.amount;
          if (this.dir === 'asc') {
            return amounta - amountb;
          }
          return amountb - amounta;
        case 'beneficiary':
          const beneficiarya = a.merchant.name;
          const beneficiaryb = b.merchant.name;
          if (this.dir === 'asc') {
            return this.comparestrings(beneficiarya, beneficiaryb);
          }
          return this.comparestrings(beneficiaryb, beneficiarya);
        }
      return 0;
    });
  }

  flipDir() {
    if (this.dir === 'asc') {
      this.dir = 'desc';
    } else {
      this.dir = 'asc';
    }
  }

}
