# TransactionsModule

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Developer notes

1. Assets were incomplete. Specifically, icons 'arrows' and 'briefcase' had white background instead of transaprent (or at least green, as shown on design). Moreover, there was a problem in mocked JSON file with data (in one object instance, date field had no name). Finally, there was no design provided for transfer preview screen.
2. Some of solutions were chosen only because this project is just a toy. Like building path to image file based on server data without any sanitization, or using server data as a CSS value also without any sanitization, which must never occur in production systems, as it could pose a security threat.
3. Further inconsistencies: design shows all money amounts in dollars, while mock data contains only euro. Thus it was unclear which currency should be chosen by default in the transfer making part of the app.
